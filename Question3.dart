class AppOfTheYear {
  String _name;
  String _teamdevs;
  String _year;
  String _sector;

  AppOfTheYear(this._name, this._year, this._sector, this._teamdevs);

  String get name => _name;
  set name(String name) {
    _name = name;
  }

  String get year => _year;
  set year(String year) {
    _year = year;
  }

  String get sector => _sector;
  set sector(String sector) {
    _sector = sector;
  }

  String get team_devs => _teamdevs;

  set teamdevs(String team_devs) {
    _teamdevs = team_devs;
  }

  void capsletter() {
    print(_name.toUpperCase());
    return;
  }

  String toString() {
    print('Name: ${this._name}');
    print('Year of Winning: ${this._year}');
    print('Sector of Business: ${this._sector}');
    print('Team/Developer: ${this._teamdevs}');
    return super.toString();
  }
}

void main() {
  AppOfTheYear fnbapp =
      AppOfTheYear("FNB Banking App", "2012", "Finance", "Frederick Kobo");

  print('A).');
  fnbapp.toString();
  print('\nB).');
  fnbapp.capsletter();
}
